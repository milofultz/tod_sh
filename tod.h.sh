#!/bin/bash

# Constants
TOD_FILE="$HOME/.tod"
SECONDS_IN_A_MINUTE=60
POMODORO_MINUTES=25
POMODORO_SECONDS=$(( POMODORO_MINUTES * SECONDS_IN_A_MINUTE ))
BREAK_MINUTES=5
BREAK_SECONDS=$(( BREAK_MINUTES * SECONDS_IN_A_MINUTE ))
ALERT="$(dirname "$0")/alert.wav"
RUNNING_TASK="/tmp/tod_task"
PIDS="/tmp/tod_timer.pid"
TEMP_FILE="/tmp/tod"
BEGIN_AWK_TSV="BEGIN { FS=OFS=\"\t\"; }"
TODAY="$(date +"%y%m%d")"
TAB=$'\t'

C_GREEN=$(tput setaf 2)
C_YELLOW=$(tput setaf 3)
C_RESET=$(tput sgr0)

# "Return" variables
TASK=""
EXISTING_PID=""

# META HELPERS

init() {
    touch "$TOD_FILE"
    touch "$RUNNING_TASK"
    touch "$PIDS"
}

# TIMER HELPERS

get_existing_pid_if_exists() {
    EXISTING_PID=$(cat "$PIDS")
}

get_task() {
    TASK="$(sed -n -e "${1}p" -e 's@\/@\\\/@g' "$TOD_FILE")"
    TASK="${TASK%%	*}" # Remove everything beyond the first tab
}

ring_alarm() {
    echo "Use command \`tod k\` to stop the alarm."

    i=0
    max=5
    while [[ $i -lt $max ]]; do
        tput flash
        afplay "$ALERT" && afplay "$ALERT" && afplay "$ALERT"
        sleep 1.5
        (( i = i + 1 ))
    done
}

do_pomodoro() {
    touch_task "$1"

    get_task "$1"
    run_timer "$TASK"
}

run_timer() {
    kill_existing_timers

    echo -e "⏱  ${POMODORO_MINUTES}m: \"$1\"."

    sleep $POMODORO_SECONDS \
    && echo -e "\n\nTIMER COMPLETE: $TASK\n" \
    && ring_alarm &

    echo $! > "$PIDS"
    echo "$1" > "$RUNNING_TASK"
}

take_break() {
    kill_existing_timers

    echo -e "⏱  ${BREAK_MINUTES}m"
    sleep $BREAK_SECONDS \
        && echo -e "\n\nTIMER COMPLETE: $TASK\n" \
        && ring_alarm &

    echo $! > "$PIDS"
}

kill_existing_timers() {
    get_existing_pid_if_exists

    if [[ -z $EXISTING_PID ]]; then
        return 0
    fi

    is_running=$(ps -p "$EXISTING_PID" | tail -n +2)

    if [[ $is_running ]]; then
        # Stop all background Tod processes. Surpress errors because
        #   if an old process is finished, there is nothing to stop
        t="$(cat "$RUNNING_TASK")"
        if [[ "${#t}" > 0 ]]; then
            echo "☠️  ⏱  \"$t\"."
        fi
        kill -9 "$EXISTING_PID" 2> /dev/null
        # Starting a timer from a shell script creates the pid for
        #   the script as well as the timer itself.
        kill "$(pgrep sleep $POMODORO_SECONDS)" 2> /dev/null
        # Reset existing PID and running task
        EXISTING_PID=""
        echo "" > $RUNNING_TASK
    fi
}

time_left() {
    get_existing_pid_if_exists

    if [[ -z $EXISTING_PID ]]; then
        return 0
    fi

    # Show elapsed time and suppress the header
    elapsed=$(ps -p "$EXISTING_PID" -o "etime=")
    task_name=$(cat "$RUNNING_TASK")
    if [[ $elapsed != "" ]]; then
        echo "${C_YELLOW}Current Task:${C_RESET} $task_name"
        echo "${C_GREEN}Time Elapsed:${C_RESET} $elapsed"
    fi
}

# TOD FILE HELPERS

add_tasks() {
    if [[ -z "$1" ]]; then
        echo "No task entered. Type \`tod h\` for help."
        exit 1
    fi

    for task in "$@"; do
        if [[ -n "$task" ]]; then
            title="${task%% +*}"
            if [[ $title == $task ]]; then
                # No project specified, so remove the due date
                title="${task% *}"
            fi
            # Get date at end of entry and remove all non numbers
            due="${task##* }"
            due="${due//[^0-9]/}"
            # If I accidentally did YYYYMMDD, trim the start of the year off
            if [[ "${#due}" -eq 8 ]]; then
                due="${due##20}"
            fi
            project=""
            if [[ "$task" =~ ' +' ]]; then
                tmp="${task##*+}"
                project="+${tmp%% *}"
            fi
            echo -e "$title\t$project\t$TODAY\t$due\t$TODAY" >> "$TOD_FILE"
        fi
    done
    list_tasks
}

add_and_do() {
    add_tasks "$1"
    run_timer "$1"
}

mark_complete() {
    get_task "$1"

    if [[ "$TASK" == $(cat "$RUNNING_TASK") ]]; then
      kill_existing_timers
    fi

    task_numbers=( "$@" )

    cp "$TOD_FILE" "$TEMP_FILE"

    for task_number in "${task_numbers[@]}"; do
        if [[ ! $task_number =~ ^[0-9]+$ ]]; then
            echo "Invalid argument: $task_number"
            exit 1
        fi

        awk "
            $BEGIN_AWK_TSV
            (FNR != $task_number) { print; next; }
            //                   { \$5 = \"$TODAY\"; }
            /^\\./               { print substr(\$0,2); next; }
            /^[^.]/              { print \".\" \$0; }
        " "$TOD_FILE" > "$TEMP_FILE"
        mv "$TEMP_FILE" "$TOD_FILE"
    done
    list_tasks "all"
}

list_tasks() {
    # Sort file by project -> complete -> due date -> last touched
    tod_by_project=$(sort -t"$TAB" -k2 "$TOD_FILE")
    completed="$( \
        echo "$tod_by_project" | \
        sed '/^[^.]/d' | \
        sort -t"$TAB" -k5 \
    )"
    tasks="$(echo "$tod_by_project" | sed '/^\./d')"
    tasks="$({\
      echo "$tasks" | awk "$BEGIN_AWK_TSV (length(\$4) > 0)"  | sort -t"$TAB" -k4 -k5; \
      echo "$tasks" | awk "$BEGIN_AWK_TSV (length(\$4) == 0)" | sort -t"$TAB" -k5; \
    })"
    echo -e "$tasks\n$completed" | awk 'NF' > "$TOD_FILE"

    if [[ "$1" == "project" ]] && [[ -z "$1" ]]; then
        echo -e "No project specified."
        exit 1
    fi

    clear

    pattern="(\$1 ~ /^[^.]/)" # all but completed tasks
    case "$1" in
        all)       pattern="//" ;;
        completed) pattern="(\$1 ~ /^\\./)" ;;
        project)   pattern="(\$1 ~ /^[^.]/ && \$2 ~ /\+$2/)" ;;
    esac

    # Add padded line number
    numbered_tasks="$(awk "$BEGIN_AWK_TSV
            $pattern {
                \$0 = sprintf(\"%-5s\t%s\", NR, \$0);
                print \$0;
            }" "$TOD_FILE")"

    printf "%s\n%s\n%s\n" \
           $'# \tTITLE \tPROJ \tSTART \tDUE   \tLAST' \
           $'--\t------\t-----\t------\t------\t------' \
           "$numbered_tasks" \
        | sed -e $'s/\t/\t /g' \
        | column -s"$TAB" -t
}

touch_task() {
    for task in "$@"; do
        awk "
            $BEGIN_AWK_TSV
            (FNR == $task) { \$5 = \"$TODAY\"; print \$0; next; }
            // { print; }
        " "$TOD_FILE" > "$TEMP_FILE" && mv "$TEMP_FILE" "$TOD_FILE"
    done
}
