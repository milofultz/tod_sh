# Tod

A simple task manager and [pomodoro](https://en.wikipedia.org/wiki/Pomodoro_Technique) timer in shell scripts.

## What it does

* Manages tasks to do
* Keeps track of task due date and when you last worked on it
* Times work in pomodoro chunks

## Installation

Clone the repo somewhere that it can be referenced by your shell. In your shell's `rc` file, add an alias of `tod=./path/to/tod.sh` so you can use it wherever. Tod will create a `.tod` file in your `$HOME` directory.

The scripts use mostly unix built-ins, focused mainly on the Mac flavor.

## Usage

Tod is used in the command line via whatever alias you created. For instance, if you set an alias to `tod`, then you would add a task with `tod a "New task"`, start a pomodoro timer with `tod 1`, list all tasks with `tod ls`, etc. Only one pomodoro can be running at a time.

### Flags

| Flag                            | Arguments               | Description                                   |
|---------------------------------|-------------------------|-----------------------------------------------|
| N/A                             |                         | List all uncompleted tasks                    |
| `ls`                            |                         | List all tasks                                |
| `n`                             | `n`: Task number        | Start pomodoro timer                          |
| `a "task"[ "another task" ...]` | `task`: New task        | Add new task.                                 |
| `aa "task"`                     | `task`: New task        | Add new task and start pomodoro timer         |
| `b`                             |                         | Start 5m break                                |
| `c n[ n ...]`                   | `n`: Task number        | Mark task as complete/incomplete              |
| `e`                             |                         | Open `.tod` file for editing                  |
| `k`                             |                         | Kill timers                                   |
| `lc` / `c`                      |                         | List all completed tasks                      |
| `p "project"`                   | `project`: Project name | List all tasks belonging to project           |
| `t`                             |                         | See elapsed time for timer                    |
| `t n[ n ...]`                   | `n`: Task number        | "Touch" task (set date last touched to today) |

Adding a task requires at least a title/description, and can contain an option project preceded by a `+` with no spaces (e.g. `+projectname`), as well as an optional due date formatted `YYMMDD` (e.g. `220121` meaning Jan 21, 2022).

### Tasks

The `.tod` file is a list of tab separated values in the following order:

* Title/description
* Project (optional)
* Date started
* Date due (optional)
* Date last touched

A `.` preceding the title/description represents a completed task.

All projects are preceded by a `+` and contain no spaces.

All dates are stored as `YYMMDD`. Date started and last touched are created and/or updated by tod.

## Future Improvements

* ~~Move all initial variables and helper functions into "helper" shell script, a la C's `h` file.~~
* ~~Add due dates and organize by soonest~~
* ~~Show error if trying to complete a non-existent task.~~
* Test suite for shell script? Sounds like a fun project.

